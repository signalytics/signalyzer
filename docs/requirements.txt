sphinx>=6.1.3
sphinx-copybutton>=0.5.2
sphinx-plotly-directive>=0.1.3
furo>=2023.03.27
